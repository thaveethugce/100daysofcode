import { Day02Page } from './app.po';

describe('day02 App', () => {
  let page: Day02Page;

  beforeEach(() => {
    page = new Day02Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
