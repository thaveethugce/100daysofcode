import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { UserComponent } from './user/user.component';
import { UserService } from './user/user.service';

import { AppRoutingModule } from './routing/app-routing.module';
import { MaterialModule, MdList, MdListItem } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    MaterialModule
  ],
  bootstrap: [AppComponent],
  providers: [UserService]
})
export class AppModule { }
