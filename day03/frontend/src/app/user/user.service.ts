import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { User } from './user';

@Injectable()
export class UserService {
	constructor(private http: Http){

	}
	private headers = new Headers({'Content-Type':'application'});
	private userUrl = 'myng/api/user/';

	getUsers(): Promise<User[]> {
		console.log("callling service");
		return this.http.get(this.userUrl).
		toPromise()
		.then(response=> response.json() as User[])
		.catch(this.handleError);
	}
	private handleError(error:any): Promise<any> {
	   console.error("An error occured ",error);
	   return Promise.reject(error.message || error);
	}
}