function flattenArrayOfArrays(a, r){
    if(!r){ r = []}
    for(var i=0; i<a.length; i++){
        if(a[i].constructor == Array){
            r.concat(flattenArrayOfArrays(a[i], r));
        }else{
            r.push(a[i]);
        }
    }
    return r;
}

function flatternArrayusingReduce(arr){
  if(!Array.isArray(arr)) {
    return arr;
  }
  return arr.reduce(function(res,par){
    return res.concat(flatternArrayusingReduce(par));
  },[]);

}