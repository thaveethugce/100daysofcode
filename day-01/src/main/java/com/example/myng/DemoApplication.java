package com.example.myng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.*;

import com.example.myng.configuration.JpaConfiguration;

import org.springframework.stereotype.*;

@Import(JpaConfiguration.class)
@SpringBootApplication(scanBasePackages={"com.example"})
public class DemoApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}